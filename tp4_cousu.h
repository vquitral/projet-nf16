#ifndef TP4_COUSU_H_INCLUDED
#define TP4_COUSU_H_INCLUDED
#include "tp 4.h"
struct Noeud_C{
    int val;
    int succ_droit;
    int succ_gauche;
    struct Noeud_C *fils_gauche;
    struct Noeud_C *fils_droit;
};

typedef struct Noeud_C T_Noeud_C;
typedef struct Noeud_C *T_Arbre_C;

T_Noeud_C *cousu_creer_noeud(int valeur);

void cousu_prefixe(T_Arbre_C arbre);
void cousu_inserer(int valeur,T_Arbre_C *arbre);
void cousu_infixe(T_Arbre_C *arbre);
void afficher_Noeud(T_Arbre_C arbre);
void abr_to_cousu(T_Arbre abr, T_Arbre_C *clone, T_Noeud_C * parent);
void cousu_memoire(T_Arbre_C arbre);

#endif // TP4_COUSU_H_INCLUDED
