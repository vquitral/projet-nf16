#include "tp4_cousu.h"
#include "tp 4.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

T_Noeud_C *cousu_creer_noeud(int valeur){
    T_Noeud_C *nouveau = malloc (sizeof (T_Noeud_C));
    nouveau->val=valeur;
    nouveau->fils_gauche=NULL;
    nouveau->fils_droit=NULL;
    nouveau->succ_droit = 1;
    nouveau->succ_gauche = 1;
    return nouveau;
}

void cousu_prefixe(T_Arbre_C arbre){
    if (arbre != NULL){
        printf ("Noeud --> %d \n", arbre->val);
        if (arbre->succ_gauche != 1)
            printf("-->FG(%d) : %d -- \t", arbre->succ_gauche, arbre->fils_gauche->val);
        else if (arbre->fils_gauche != NULL)
            printf("FG(%d) : %d -- \t", arbre->succ_gauche, arbre->fils_gauche->val);
        else if (arbre->fils_gauche == NULL)
            printf("FG(%d) :NULL -- \t", arbre->succ_gauche);

        if (arbre->succ_droit != 1)
            printf ("-> FD(%d) : %d \n", arbre->succ_droit, arbre->fils_droit->val);
        else if (arbre->fils_droit != NULL)
            printf("FD(%d) : %d \n",arbre->succ_droit,arbre->fils_droit->val);
         else if (arbre->fils_droit == NULL)
            printf("FD(%d) :NULL -- \t", arbre->succ_droit);
        if(arbre->succ_gauche != 1)
        cousu_prefixe(arbre->fils_gauche);
        if(arbre->succ_droit != 1)
        cousu_prefixe(arbre->fils_droit);


}
}

void cousu_inserer(int valeur,T_Arbre_C *arbre){
    T_Arbre_C pere;
    T_Noeud_C *nouvo = cousu_creer_noeud(valeur);
    T_Arbre_C x = *arbre;
    int a = 1; //pour v�rifier si c'est effectivement un fils
    if (arbre == NULL){ //si l'arbre est vide, on cr�e une racine = valeur
        *arbre  = cousu_creer_noeud(valeur);
        return;
    }
    while (x!= NULL && a !=0){
        if (valeur == x->val){ //si la valeur existe d�j�
            printf("la valeur %d est deja presente dans l'arbre \n", valeur);
            return;
        }

        pere=x;

        if (valeur < x->val){
            if(x->succ_gauche == 0){
                x= x->fils_gauche;
            }
            else { //cas o� le fils gauche est un predecesseur
                a = 0;
            }

        }else{
            if(x->succ_droit == 0){
                x= x->fils_droit;
            }
            else { //cas o� le fils droit est un successeur
                a = 0;
            }
        }
    }
    if (valeur < pere->val){

        nouvo->fils_droit = pere;
        nouvo->fils_gauche = pere->fils_gauche;
        pere->succ_gauche=0;
        pere->fils_gauche = nouvo;

    }
    if (valeur > pere->val){

        nouvo->fils_gauche=pere;
        nouvo->fils_droit=pere->fils_droit;
        pere->succ_droit=0;
        pere->fils_droit=nouvo;
    }
}

void cousu_infixe(T_Arbre_C *arbre){
    T_Arbre_C cousu = *arbre;
    if(cousu == NULL){
        printf("Erreur, arbre vide");
        return;
    }
    else {//Si l'arbre n'est pas vide
        //On commence le parcours par le n�ud le plus � gauche de l�arbre cousu
        while(cousu->succ_gauche != 1){
              cousu = cousu->fils_gauche;
        }

        while(cousu!=NULL){
            afficher_Noeud(cousu);//Si le n�ud courant n�est pas NULL alors l�afficher
            if(cousu->succ_droit == 1){ //Sic'est 1 alors le successeur droit est son p�re
                cousu = cousu->fils_droit;
            }
            else{
                cousu = cousu->fils_droit; //alors on cherche son successeur infixe (n�ud le plus � gauche dans son sous-arbre droit)
                 while(cousu->succ_gauche != 1){
                      cousu = cousu->fils_gauche;   //on r�cup�re l'�l�ment le plus a gauche
                }

            }
        }
    }

}





void afficher_Noeud(T_Arbre_C arbre){
        printf ("Noeud --> %d \n", arbre->val);
        if (arbre->succ_gauche != 1)
            printf("-->FG(%d) : %d -- \t", arbre->succ_gauche, arbre->fils_gauche->val);
        else if (arbre->fils_gauche != NULL)
            printf("FG(%d) : %d -- \t", arbre->succ_gauche, arbre->fils_gauche->val);
        else if (arbre->fils_gauche == NULL)
            printf("FG(%d) :NULL -- \t", arbre->succ_gauche);
        if (arbre->succ_droit != 1)
            printf ("-> FD(%d) : %d \n", arbre->succ_droit, arbre->fils_droit->val);
        else if (arbre->fils_droit != NULL)
            printf("FD(%d) : %d \n",arbre->succ_droit,arbre->fils_droit->val);
         else if (arbre->fils_droit == NULL)
            printf("FD(%d) :NULL -- \t", arbre->succ_droit);
}


void abr_to_cousu (T_Arbre abr, T_Arbre_C *clone, T_Noeud_C * parent) {

    if (abr!= NULL){
            T_Noeud_C *tmp = cousu_creer_noeud(abr->val);


            //on va affecter � parent le fils gauche et le fils droit, sauf dans le cas de la premi�re it�ration o� parent= NIL

            //pour savoir si le noeud en question est le fils gauche ou le fils droit du parent , on va comparer la valeur de leur noeud

            if (parent != NULL){    //on n'est pas en train de traiter la racine
                if (parent->val < (tmp) -> val){  //alors clone est le fils droit du parent

                    (tmp)->fils_gauche=parent;  //predecesseur est son pere
                    (tmp)->fils_droit=parent->fils_droit;
                    parent->fils_droit = tmp;
                    parent->succ_droit = 0; //le parent a maintenant un fils driot
                }

                else if (parent->val > (tmp) -> val){ //alors clone est le fils gauche du parent

                    (tmp)->fils_droit=parent;
                    (tmp)->fils_gauche=parent->fils_gauche;
                    parent->fils_gauche= tmp;
                    parent->succ_gauche=0;
                }
            }
            *clone = tmp;

            if (abr->fils_gauche != NULL){
                abr_to_cousu(abr->fils_gauche, &((*clone)->fils_gauche), *clone);
            }
            if (abr->fils_droit != NULL){
                abr_to_cousu(abr->fils_droit, &((*clone)->fils_droit), *clone);
            }

        }
}

void cousu_memoire(T_Arbre_C arbre){
    T_Arbre_C fils_gauche, fils_droit;
        if (arbre != NULL){
        //On commence le parcours par le n�ud le plus � gauche de l�arbre cousu
        while(arbre->succ_gauche != 1){
              arbre = arbre->fils_gauche;
        }
        fils_gauche = arbre->fils_gauche;
        fils_droit = arbre->fils_droit;
        free(arbre);
        cousu_memoire(fils_droit);
        cousu_memoire(fils_gauche);
        }
}
