

#ifndef TP4_H_INCLUDED
#define TP4_H_INCLUDED

struct noeud{
    int val;
    struct noeud *fils_droit;
    struct noeud *fils_gauche;
};

typedef struct noeud T_Noeud;
typedef struct noeud *T_Arbre;


T_Noeud *abr_creer_noeud (int val);


void abr_prefixe (T_Arbre abr);
void abr_inserer (int valeur, T_Arbre *abr); //valeur � ins�rer

void abr_supprimer (int valeur, T_Arbre *abr);
void abr_clone (T_Arbre original, T_Arbre *clone, T_Noeud *parent);
void abr_memoire (T_Arbre abr);


#endif // TP4_H_INCLUDED
