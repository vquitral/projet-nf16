#include <stdio.h>
#include <stdlib.h>
#include "tp 4.h"
#include "tp4_cousu.h"

/*int main() //Utilisé pour tester la fonction
{
    T_Arbre T1 = abr_creer_noeud(11);
    T_Arbre T2;
    T_Noeud *parent= NULL;
    //abr_inserer(11, &T1);
    abr_inserer(13, &T1);
    abr_inserer(1, &T1);
    abr_inserer(6, &T1);
    abr_inserer(17, &T1);
    abr_inserer(15, &T1);
    //abr_supprimer(11, &T1);
    //abr_prefixe(T1);
    abr_clone(T1, &T2, parent);
    //abr_prefixe(T2);

    T_Arbre_C N1 = cousu_creer_noeud(11);
    T_Arbre_C N2;
    printf("djdj");
    cousu_inserer(11, &N1);
    cousu_inserer(2, &N1);
    cousu_inserer(1, &N1);
    cousu_inserer(13, &N1);
    cousu_inserer(6, &N1);
    cousu_inserer(17, &N1);
    cousu_inserer(15, &N1);
    printf("lol");
    //cousu_prefixe(N1);
    //cousu_infixe(&N1);
    abr_to_cousu(T1, &N2, NULL);
    cousu_prefixe(N2);



}*/

int main(){
    int choix = 0, valeur;
    T_Arbre ABR1,ABRClone;
    T_Arbre_C COUSU1;

    while (choix != 11){
        printf("--------------------------------------------------\n");
        printf("--------------------------------------------------\n");
        printf("1. Creer un ABR\n");
        printf("2. Afficher l ABR en prefixe \n");
        printf("3. Inserer une valeur dans l ABR \n");
        printf("4. Supprimer une valeur de l ABR \n");
        printf("5. Cloner l ABR \n");
        printf("6. Afficher le clone en prefixe \n");
        printf("7. Creer un arbre binaire cousu a partir d un ABR\n");
        printf("8. Afficher l arbre binaire cousu en prefixe \n");
        printf("9. Afficher l arbre binaire cousu en infixe \n");
        printf("10. Inserer une valeur dans l arbre binaire cousu \n");
        printf("11. Quitter \n");
        printf("--------------------------------------------------\n");
        printf("--------------------------------------------------\n");
        printf("Quel est votre choix ? ");

        scanf("%d", &choix);
        printf("\n");

        switch(choix){

            case 1:
                printf("Entrez une valeur : ");
                scanf("%d", &valeur);
                printf("\n");
                ABR1 = abr_creer_noeud(valeur);

            break;

            case 2:
                printf("Affichage de l arbre en préfixe : \n");
                if(ABR1 == NULL){
                    printf("Erreur, l arbre est vide\n");
                    break;
                }
                abr_prefixe(ABR1);
            break;

            case 3:
                printf("Entrez une valeur : ");
                scanf("%d", &valeur);
                printf("\n");
                abr_inserer(valeur, &ABR1);
            break;

            case 4:
                printf("Entrez la valeur a supprimer : ");
                scanf("%d", &valeur);
                printf("\n");
                abr_supprimer(valeur, &ABR1);
            break;

            case 5:
                printf("Vous avez choisi de cloner l arbre. Creation du clone en cours.");
                printf("\n");
                abr_clone(ABR1, &ABRClone,NULL);

            break;

            case 6 :
                printf("Affichage de l arbre clone en prefixe");
                printf("\n");
                abr_prefixe(ABRClone);
            break;

            case 7 :
                printf("Vous allez creer un arbre cousu a partir de l arbre binaire");
                printf("\n");
                abr_to_cousu(ABR1, &COUSU1, NULL);
            break;

            case 8 :
                printf("Affichage de l arbre cousu en prefixe");
                printf("\n");
                cousu_prefixe(COUSU1);
            break;

            case 9:
                printf("Affichage de l arbre cousu en infixe");
                printf("\n");
                cousu_infixe(&COUSU1);
            break;

            case 10 :
                printf("Entrez une valeur : ");
                scanf("%d", &valeur);
                printf("\n");
                cousu_inserer(valeur, &COUSU1);
            break;

            case 11:
                return 0;
            break;

            default :
                printf("vous n avez pas entre une option correcte");
            break;
        }
    }
    cousu_memoire(COUSU1);
    abr_memoire(ABR1);
    abr_memoire(ABRClone);
    return 0;
}