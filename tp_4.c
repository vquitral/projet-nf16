#include "tp 4.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


T_Noeud *abr_creer_noeud (int val){
    T_Noeud *newnoeud = malloc (sizeof (T_Noeud));
    newnoeud->val=val;
    newnoeud->fils_gauche=NULL;
    newnoeud->fils_droit=NULL;
    return newnoeud;
}

void abr_prefixe (T_Arbre abr){ //Affichage pr�fixe de l'arbre
    if (abr != NULL){
        printf ("Noeud --> %d \n", abr->val);
        if (abr->fils_gauche != NULL)
            printf("-->FG : %d -- \t", abr->fils_gauche->val);
        else
            printf("FG : NULL -- \t");

        if (abr->fils_droit != NULL)
            printf ("-> FD : %d \n", abr->fils_droit->val);
        else
            printf("FD : NULL \n");
        abr_prefixe(abr->fils_gauche);
        abr_prefixe(abr->fils_droit);

    }
}

void abr_inserer (int valeur, T_Arbre *abr){ //valeur � ins�rer dans abr
    T_Arbre pere;
    T_Noeud *nouvo = abr_creer_noeud(valeur);
    T_Arbre x = *abr; //x a le contenu de abr
    if (abr == NULL){ //si l'arbre est vide, on cr�e une racine = valeur
        *abr  = abr_creer_noeud(valeur);
        return;
    }
    while (x!= NULL){
        if (valeur == x->val){ //si la valeur existe d�j�
            printf("la valeur %d est deja presente dans l'arbre \n", valeur);
            return;
        }

        pere=x;
        if (valeur < x->val){
            x= x->fils_gauche;
        }else
            x=x->fils_droit;
    }
    if (valeur < pere->val)
        pere->fils_gauche = nouvo;
    if (valeur > pere->val)
        pere->fils_droit=nouvo;
}

void abr_supprimer (int valeur, T_Arbre *abr){
    T_Arbre pere=NULL;;
    T_Arbre x = *abr;
    while (x!= NULL && x->val != valeur){
        pere=x;
        if (valeur < x->val){
            x= x->fils_gauche;
        }else
            x=x->fils_droit;
    }
    if (x == NULL){
        printf("l'�l�ment n'est pas pr�sent dans l'arbre \n");
    }
    else {
        if (x->fils_gauche == NULL && x->fils_droit == NULL){ //cas 1 : suppression d'une feuille
            if (pere == NULL)
                *abr = NULL;
            else if (valeur < pere->val){
                pere->fils_gauche = NULL;
            }else
                pere->fils_droit = NULL;
            free(x);
            }

        else if (x->fils_gauche == NULL && x->fils_droit != NULL){  //cas 2 a) le noeud n'a pas de fils gauche
            if (pere == NULL)
                *abr = x->fils_droit;
            else if (pere->val < x->fils_droit->val){
                pere->fils_droit=x->fils_droit;
            }else
                pere->fils_gauche=x->fils_droit;
            free(x);
            }

        else if (x->fils_droit == NULL && x->fils_gauche != NULL){ //cas 2 b) le noeud n'a pas de fils droit
             if (pere == NULL)
                *abr = x->fils_gauche;
            else if (pere->val < x->fils_gauche->val){
                pere->fils_droit=x->fils_gauche;
            }else
                pere->fils_gauche=x->fils_gauche;
            free(x);
            }
        else if (x->fils_droit != NULL && x->fils_gauche != NULL){ //cas 3 : rien besoin de changer
            T_Arbre successeur = x->fils_droit;
            while (successeur -> fils_gauche != NULL)
                successeur= successeur->fils_gauche;
            x->val = successeur->val;
            abr_supprimer(x->val, &x->fils_droit);

        }
        }
    }



void abr_clone (T_Arbre original, T_Arbre *clone, T_Noeud *parent){
        if (original!= NULL){
            *clone = abr_creer_noeud(original->val);

            //on va affecter � parent le fils gauche et le fils droit, sauf dans le cas de la premi�re it�ration o� parent= NIL

            //pour savoir si le noeud en question est le fils gauche ou le fils droit du parent , on va comparer la valeur de leur noeud

            if (parent != NULL){
                if (parent->val < (*clone) -> val){  //alors clone est le fils droit du parent
                    parent->fils_droit = *clone;
                }
                else if (parent->val > (*clone) -> val){ //alors clone est le fils gauche du parent
                    parent->fils_gauche= *clone;
                }
            }

            if (original->fils_gauche != NULL){
                abr_clone(original->fils_gauche, &((*clone)->fils_gauche), *clone);
            }
            if (original->fils_droit != NULL){
                abr_clone(original->fils_droit, &((*clone)->fils_droit), *clone);
            }
        }
}

void abr_memoire (T_Arbre abr){ //Affichage pr�fixe de l'arbre
    if(abr != NULL){
    T_Arbre fils_gauche = abr->fils_gauche;
    T_Arbre fils_droit = abr->fils_droit;
    free(abr);
    abr_memoire(fils_gauche);
    abr_memoire(fils_droit);
    }
}



